const express = require('express');
const app = express();

const sort = (arr) => {
  for (let i = 1; i < arr.length; i++) {
    let key = arr[i];
    let j = i - 1;
    while (j >= 0 && key < arr[j]) { 
      arr[j + 1] = arr[j];
      j--;
    }
    arr[j + 1] = key;
  }
  return arr;
}

app.get('/sort', (req, res) => {
  // convert req.query.values to an array
  const values = req.query.values.split(',').map(val => parseInt(val));  
  // sends back the values *sorted* through the HTTP response
  res.send(sort(values).join(','));
})

const server = app.listen(3000, () => {
  console.log('app listening on port 3000!');
});

app.close = (done) => {
  server.close(done);
}

module.exports = app